package com.sonarsource;

public class ArchiverException extends Exception {
    public ArchiverException(String message) {
        super(message);
    }
}
